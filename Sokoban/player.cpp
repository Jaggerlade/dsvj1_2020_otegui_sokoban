#include "player.h"
#include "grid.h"
#include "screen.h"
#include <iostream>
using namespace std;
namespace Sokoban 
{
	namespace player 
	{
		PLAYER player;
		Texture2D playerTextureArr[4];
		void init()
		{
			initFirst();
			loadTexture();
		}

		void update()
		{
			
		}

		void draw()
		{		
			if (screen::state == screen::playing)
			{
				DrawTexture(playerTextureArr[player.playerDirection], static_cast<int> (player.rectangle.x), static_cast<int> (player.rectangle.y), WHITE);
			}
															
		}

		void deInit()
		{
			unLoadTexture();
		}

		void playerMovementToWalkable()
		{
			
			if (IsKeyPressed(KEY_W))
			{
				if (grid::gridArr[player.filas][player.columnas - 1].squareType == grid::walkable || grid::gridArr[player.filas][player.columnas - 1].squareType == grid::point)
				{
					player.playerDirection = up;
					grid::gridArr[player.filas][player.columnas - 1].squareType = grid::player;

					grid::gridArr[player.filas][player.columnas].squareType = grid::walkable;

					player.columnas -= 1;
					player.movimientos++;
				}
					
				else if (grid::gridArr[player.filas][player.columnas - 1].squareType == grid::box && (grid::gridArr[player.filas][player.columnas - 2].squareType == grid::walkable
					|| grid::gridArr[player.filas][player.columnas - 2].squareType == grid::point))
				{
					player.playerDirection = up;
					grid::gridArr[player.filas][player.columnas - 1].squareType = grid::player;
					grid::gridArr[player.filas][player.columnas - 2].squareType = grid::box;

					grid::gridArr[player.filas][player.columnas].squareType = grid::walkable;

					player.columnas -= 1;
					player.movimientos++;
					
				}
			}

			if (IsKeyPressed(KEY_S))
			{
				if (grid::gridArr[player.filas][player.columnas + 1].squareType == grid::walkable || grid::gridArr[player.filas][player.columnas + 1].squareType == grid::point)
				{
					player.playerDirection = down;
					grid::gridArr[player.filas][player.columnas + 1].squareType = grid::player;

					grid::gridArr[player.filas][player.columnas].squareType = grid::walkable;

					player.columnas += 1;
					player.movimientos++;
				}
				
				else if (grid::gridArr[player.filas][player.columnas + 1].squareType == grid::box && (grid::gridArr[player.filas][player.columnas + 2].squareType == grid::walkable
					|| grid::gridArr[player.filas][player.columnas + 2].squareType == grid::point))
				{
					player.playerDirection = down;
					grid::gridArr[player.filas][player.columnas + 1].squareType = grid::player;
					grid::gridArr[player.filas][player.columnas + 2].squareType = grid::box;

					grid::gridArr[player.filas][player.columnas].squareType = grid::walkable;

					player.columnas += 1;
					player.movimientos++;
				}
			}

			if (IsKeyPressed(KEY_A))
			{
				if (grid::gridArr[player.filas - 1][player.columnas].squareType == grid::walkable || grid::gridArr[player.filas - 1][player.columnas].squareType == grid::point)
				{
					player.playerDirection = left;
					grid::gridArr[player.filas - 1][player.columnas].squareType = grid::player;

					grid::gridArr[player.filas][player.columnas].squareType = grid::walkable;

					player.filas -= 1;
					player.movimientos++;
				}
				
				else if (grid::gridArr[player.filas - 1][player.columnas].squareType == grid::box && (grid::gridArr[player.filas - 2][player.columnas].squareType == grid::walkable
					|| grid::gridArr[player.filas - 2][player.columnas].squareType == grid::point))
				{
					player.playerDirection = left;
					grid::gridArr[player.filas - 1][player.columnas].squareType = grid::player;
					grid::gridArr[player.filas - 2][player.columnas].squareType = grid::box;
					grid::gridArr[player.filas][player.columnas].squareType = grid::walkable;

					player.filas -= 1;
					player.movimientos++;
				}
			}

			if (IsKeyPressed(KEY_D))
			{
				if (grid::gridArr[player.filas + 1][player.columnas].squareType == grid::walkable || grid::gridArr[player.filas + 1][player.columnas].squareType == grid::point)
				{
					player.playerDirection = right;
					grid::gridArr[player.filas + 1][player.columnas].squareType = grid::player;

					grid::gridArr[player.filas][player.columnas].squareType = grid::walkable;

					player.filas += 1;
					player.movimientos++;
				}
				
				else if (IsKeyPressed(KEY_D) && grid::gridArr[player.filas + 1][player.columnas].squareType == grid::box && (grid::gridArr[player.filas + 2][player.columnas].squareType == grid::walkable
					|| grid::gridArr[player.filas + 2][player.columnas].squareType == grid::point))
				{
					player.playerDirection = right;
					grid::gridArr[player.filas + 1][player.columnas].squareType = grid::player;
					grid::gridArr[player.filas + 2][player.columnas].squareType = grid::box;
					grid::gridArr[player.filas][player.columnas].squareType = grid::walkable;

					player.filas += 1;
					player.movimientos++;
				}
			}	

		}

		void playerMovementWithBox()
		{
			/*
			if (IsKeyPressed(KEY_W) && grid::gridArr[player.filas][player.columnas - 1].squareType == grid::box && (grid::gridArr[player.filas][player.columnas -2].squareType == grid::walkable 
				|| grid::gridArr[player.filas][player.columnas - 2].squareType == grid::point))
			{
				player.playerDirection = up;
				grid::gridArr[player.filas][player.columnas - 1].squareType = grid::player;
				grid::gridArr[player.filas][player.columnas - 2].squareType = grid::box;

				grid::gridArr[player.filas][player.columnas].squareType = grid::walkable;

				player.columnas -= 1;
				cout << " POS Y: " << player.columnas << endl;
			}
			*/
			

			

			
		}

		void loadTexture()
		{
			Image reSize;

			reSize = LoadImage("res/Assets/Player/up.png");
			ImageResize(&reSize, (int)grid::grid.recShape.width, (int)grid::grid.recShape.height);
			playerTextureArr[up] = LoadTextureFromImage(reSize);
			UnloadImage(reSize);

			reSize = LoadImage("res/Assets/Player/down.png");
			ImageResize(&reSize, (int)grid::grid.recShape.width, (int)grid::grid.recShape.height);
			playerTextureArr[down] = LoadTextureFromImage(reSize);
			UnloadImage(reSize);

			reSize = LoadImage("res/Assets/Player/left.png");
			ImageResize(&reSize, (int)grid::grid.recShape.width, (int)grid::grid.recShape.height);
			playerTextureArr[left] = LoadTextureFromImage(reSize);
			UnloadImage(reSize);

			reSize = LoadImage("res/Assets/Player/right.png");
			ImageResize(&reSize, (int)grid::grid.recShape.width, (int)grid::grid.recShape.height);
			playerTextureArr[right] = LoadTextureFromImage(reSize);
			UnloadImage(reSize);
		}

		void unLoadTexture()
		{
			for (int i = 0; i < 4; i++)
			{
				UnloadTexture(playerTextureArr[i]);
			}
		}
		void initFirst()
		{
			player.filas = 1;
			player.columnas = 5;
			
		}
		void initSecond()
		{
			player.filas = 5;
			player.columnas = 2;
			
		}
		void initThird()
		{
			player.filas = 4;
			player.columnas = 5;
		}
		void initFourth()
		{
			player.filas = 1;
			player.columnas = 5;
		}
		void initFifth()
		{
			player.filas = 3;
			player.columnas = 3;
		}
	}
}

