#ifndef PLAYER_H
#define PLAYER_H
#include "raylib.h"
namespace Sokoban 
{
	namespace player 
	{
		enum PLAYERDIREC
		{
			up,
			down,
			left,
			right
		};
		struct PLAYER
		{
			PLAYERDIREC playerDirection;
			Rectangle rectangle;
			int speed;
			int filas;
			int columnas;
			int movimientos;
		};
		extern PLAYER player;
		extern Texture2D playerTextureArr[4];
		void init();
		void update();
		void draw();
		void deInit();
		void playerMovementToWalkable();
		void playerMovementWithBox();
		void loadTexture();
		void unLoadTexture();
		void initFirst();
		void initSecond();
		void initThird();
		void initFourth();
		void initFifth();
	}
}
#endif // !PLAYER_H


