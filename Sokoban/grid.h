#ifndef GRID_H
#define GRID_H
#include "raylib.h"
#define BACKGROUND       CLITERAL(Color){ 64, 157, 181, 255 }
namespace Sokoban 
{
	const int tamX = 7;
	const int tamY = 7;
	namespace grid 	
	{
		enum SQUARETYPE
		{			
			walkable,
			notwalkable,
			box,
			point,
			boxOnPoint,
			player,
			empty
		};
		struct GRID
		{
			SQUARETYPE squareType;
			Rectangle recShape;
			Color squareColor;
			bool onPoint;
		};
		extern GRID gridArr[tamX][tamY];
		extern GRID grid;
		extern Color colorArr[5];
		extern Texture2D textureArr[5];
		extern int onPointAmount;

		void init();
		void update();
		void draw();
		void deInit();
		void loadTexture();
		void unloadTexture();
		void initFirst();
		void initSecond();
		void initThird();
		void initFourth();
		void initFifth();
	}
}
#endif // !GRID_H

