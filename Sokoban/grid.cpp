#include "grid.h"
#include "player.h"
#include "screen.h"
#include <iostream>
using namespace std;

namespace Sokoban
{
	namespace grid
	{
		struct Vector2Int
		{
			int x;
			int y;
		};
		GRID gridArr[tamX][tamY];
		GRID grid;
		Color colorArr[5] = { RED,BLUE,GREEN,GRAY,BROWN };
		Texture2D textureArr[5];
		static Vector2 startPos;
		static Vector2Int point_1;
		static Vector2Int point_2;
		static Vector2Int point_3;
		int onPointAmount;
		static bool exit;
		static int flagToExit;
		void init()
		{
			
			initFirst();
			loadTexture();
		}
		void update()
		{
			if (screen::state == screen::playing)
			{
				onPointAmount = 0;
				player::player.rectangle = gridArr[player::player.filas][player::player.columnas].recShape;
				player::playerMovementToWalkable();
				cout << "Movimientos: " << player::player.movimientos << endl;
				//player::playerMovementWithBox();
				if (gridArr[point_1.x][point_1.y].squareType == walkable)
				{
					gridArr[point_1.x][point_1.y].squareType = point;
				}
				if (gridArr[point_2.x][point_2.y].squareType == walkable)
				{
					gridArr[point_2.x][point_2.y].squareType = point;
				}
				if (gridArr[point_3.x][point_3.y].squareType == walkable)
				{
					gridArr[point_3.x][point_3.y].squareType = point;
				}
				
				//--------------------------------------------------------------------
				if (gridArr[point_1.x][point_1.y].squareType == box)
				{
					gridArr[point_1.x][point_1.y].onPoint = true;					
					onPointAmount++;																
				}
				
				else if (gridArr[point_1.x][point_1.y].squareType != box)
				{
					gridArr[point_1.x][point_1.y].onPoint = false;
				}

				if (gridArr[point_2.x][point_2.y].squareType == box)
				{
					gridArr[point_2.x][point_2.y].onPoint = true;
					onPointAmount++;
				}
				
				else if (gridArr[point_2.x][point_2.y].squareType != box)
				{
					gridArr[point_2.x][point_2.y].onPoint = false;
				}
				
				if (gridArr[point_3.x][point_3.y].squareType == box)
				{
					gridArr[point_3.x][point_3.y].onPoint = true;
					onPointAmount++;
				}
				
				else if (gridArr[point_3.x][point_3.y].squareType != box)
				{
					gridArr[point_3.x][point_3.y].onPoint = false;
				}
				
				cout << "cantidad: " << onPointAmount << endl;
				if (onPointAmount == 3 && exit == false)
				{
					screen::state = screen::victory;
					exit = true;
					onPointAmount = 0;
					
					for (int i = 0; i < tamX; i++)
					{
						for (int j = 0; j < tamY; j++)
						{
							if (gridArr[i][j].onPoint == true)
							{
								gridArr[i][j].onPoint = false;
							}
						}
					}
				}				
			}
		}
		void draw()
		{
			if (screen::state == screen::playing)
			{
				for (int i = 0; i < tamX; i++)
				{
					for (int j = 0; j < tamY; j++)
					{
						if (gridArr[i][j].squareType != empty && gridArr[i][j].squareType != player)
						{
							DrawTexture(textureArr[gridArr[i][j].squareType], static_cast<int> (gridArr[i][j].recShape.x), static_cast<int> (gridArr[i][j].recShape.y), WHITE);
							if (gridArr[point_1.x][point_1.y].squareType == box)
							{
								DrawTexture(textureArr[4], static_cast<int> (gridArr[point_1.x][point_1.y].recShape.x), static_cast<int> (gridArr[point_1.x][point_1.y].recShape.y), WHITE);
							}
							if (gridArr[point_2.x][point_2.y].squareType == box)
							{
								DrawTexture(textureArr[4], static_cast<int> (gridArr[point_2.x][point_2.y].recShape.x), static_cast<int> (gridArr[point_2.x][point_2.y].recShape.y), WHITE);
							}
							if (gridArr[point_3.x][point_3.y].squareType == box)
							{
								DrawTexture(textureArr[4], static_cast<int> (gridArr[point_3.x][point_3.y].recShape.x), static_cast<int> (gridArr[point_3.x][point_3.y].recShape.y), WHITE);
							}
						}													
					}
				}
			}
		}
		void deInit()
		{
			unloadTexture();
		}
		void loadTexture()
		{
			Image reSize;

			reSize = LoadImage("res/Assets/Tiles/walkable.png");
			ImageResize(&reSize, (int)grid.recShape.width, (int)grid.recShape.height);
			textureArr[walkable] = LoadTextureFromImage(reSize);
			UnloadImage(reSize);

			reSize = LoadImage("res/Assets/Tiles/notwalkable.png");
			ImageResize(&reSize, (int)grid.recShape.width, (int)grid.recShape.height);
			textureArr[notwalkable] = LoadTextureFromImage(reSize);
			UnloadImage(reSize);

			reSize = LoadImage("res/Assets/Tiles/box.png");
			ImageResize(&reSize, (int)grid.recShape.width, (int)grid.recShape.height);
			textureArr[box] = LoadTextureFromImage(reSize);
			UnloadImage(reSize);

			reSize = LoadImage("res/Assets/Tiles/point.png");
			ImageResize(&reSize, (int)grid.recShape.width, (int)grid.recShape.height);
			textureArr[point] = LoadTextureFromImage(reSize);
			UnloadImage(reSize);

			reSize = LoadImage("res/Assets/Tiles/boxGreen.png");
			ImageResize(&reSize, (int)grid.recShape.width, (int)grid.recShape.height);
			textureArr[boxOnPoint] = LoadTextureFromImage(reSize);
			UnloadImage(reSize);
		}
		void unloadTexture()
		{
			for (int i = 0; i < 5; i++)
			{
				UnloadTexture(textureArr[i]);
			}
		}
		void initFirst()
		{
			player::player.movimientos = 0;
			flagToExit = 0;
			exit = false;
			point_1 = { 5,4 };
			point_2 = { 5,5 };
			point_3 = { 4,5 };

			onPointAmount = 0;
			grid.recShape.width = 100;
			grid.recShape.height = 100;
			startPos = { ((GetScreenWidth() * 0.5f) - ((grid.recShape.width * tamX)*0.5f)), ((GetScreenHeight() * 0.5f) - ((grid.recShape.height * tamY)*0.5f)) };

			for (int i = 0; i < tamX; i++)
			{
				for (int j = 0; j < tamY; j++)
				{

					gridArr[i][j].recShape.width = grid.recShape.width;
					gridArr[i][j].recShape.height = grid.recShape.height;

					gridArr[i][j].recShape.x = startPos.x + ((grid.recShape.width)*i);
					gridArr[i][j].recShape.y = startPos.y + ((grid.recShape.height)*j);

					if (i == 0 || i == tamX - 1 || j == 0 || j == tamY - 1)
					{
						gridArr[i][j].squareType = notwalkable;
					}
					else
					{
						gridArr[i][j].squareType = walkable;
					}
					if (i == 1 && j == 5)
					{
						player::player.rectangle = { startPos.x + ((grid.recShape.width)*i) ,startPos.y + ((grid.recShape.height)*j),grid.recShape.width,grid.recShape.height };
					}
				}
			}
			gridArr[1][5].squareType = player;
			gridArr[1][5].recShape = player::player.rectangle;

			gridArr[2][2].squareType = box;
			gridArr[2][4].squareType = box;
			gridArr[5][2].squareType = box;

			gridArr[point_1.x][point_1.y].squareType = point;
			gridArr[point_1.x][point_1.y].onPoint = false;
			gridArr[point_2.x][point_2.y].squareType = point;
			gridArr[point_2.x][point_2.y].onPoint = false;
			gridArr[point_3.x][point_3.y].squareType = point;
			gridArr[point_3.x][point_3.y].onPoint = false;


			gridArr[3][3].squareType = notwalkable;
			gridArr[3][4].squareType = notwalkable;
			gridArr[3][5].squareType = notwalkable;
		}
		void initSecond()
		{
			player::player.movimientos = 0;
			exit = false;
			point_1 = { 3,1 };
			point_2 = { 2,4 };
			point_3 = { 2,5 };

			onPointAmount = 0;
			grid.recShape.width = 100;
			grid.recShape.height = 100;
			startPos = { ((GetScreenWidth() * 0.5f) - ((grid.recShape.width * tamX)*0.5f)), ((GetScreenHeight() * 0.5f) - ((grid.recShape.height * tamY)*0.5f)) };

			for (int i = 0; i < tamX; i++)
			{
				for (int j = 0; j < tamY; j++)
				{

					gridArr[i][j].recShape.width = grid.recShape.width;
					gridArr[i][j].recShape.height = grid.recShape.height;

					gridArr[i][j].recShape.x = startPos.x + ((grid.recShape.width)*i);
					gridArr[i][j].recShape.y = startPos.y + ((grid.recShape.height)*j);

					if (i == 0 || i == tamX - 1 || j == 0 || j == tamY - 1)
					{
						gridArr[i][j].squareType = notwalkable;
					}
					else
					{
						gridArr[i][j].squareType = walkable;
					}
					if (i == 5 && j == 2)
					{
						player::player.rectangle = { startPos.x + ((grid.recShape.width)*i) ,startPos.y + ((grid.recShape.height)*j),grid.recShape.width,grid.recShape.height };
					}
				}
			}
			gridArr[5][2].squareType = player;
			gridArr[5][2].recShape = player::player.rectangle;

			gridArr[3][2].squareType = box;
			gridArr[4][2].squareType = box;
			gridArr[4][3].squareType = box;

			gridArr[point_1.x][point_1.y].squareType = point;
			gridArr[point_1.x][point_1.y].onPoint = false;
			gridArr[point_2.x][point_2.y].squareType = point;
			gridArr[point_2.x][point_2.y].onPoint = false;
			gridArr[point_3.x][point_3.y].squareType = point;
			gridArr[point_3.x][point_3.y].onPoint = false;


			gridArr[1][4].squareType = notwalkable;
			gridArr[3][4].squareType = notwalkable;
			gridArr[3][5].squareType = notwalkable;
			gridArr[5][1].squareType = notwalkable;
		}
		void initThird()
		{
			player::player.movimientos = 0;
			exit = false;
			point_1 = { 5,1 };
			point_2 = { 1,1 };
			point_3 = { 5,3 };

			onPointAmount = 0;
			grid.recShape.width = 100;
			grid.recShape.height = 100;
			startPos = { ((GetScreenWidth() * 0.5f) - ((grid.recShape.width * tamX)*0.5f)), ((GetScreenHeight() * 0.5f) - ((grid.recShape.height * tamY)*0.5f)) };

			for (int i = 0; i < tamX; i++)
			{
				for (int j = 0; j < tamY; j++)
				{

					gridArr[i][j].recShape.width = grid.recShape.width;
					gridArr[i][j].recShape.height = grid.recShape.height;

					gridArr[i][j].recShape.x = startPos.x + ((grid.recShape.width)*i);
					gridArr[i][j].recShape.y = startPos.y + ((grid.recShape.height)*j);

					if (i == 0 || i == tamX - 1 || j == 0 || j == tamY - 1)
					{
						gridArr[i][j].squareType = notwalkable;
					}
					else
					{
						gridArr[i][j].squareType = walkable;
					}
					if (i == 4 && j == 5)
					{
						player::player.rectangle = { startPos.x + ((grid.recShape.width)*i) ,startPos.y + ((grid.recShape.height)*j),grid.recShape.width,grid.recShape.height };
					}
				}
			}
			gridArr[4][5].squareType = player;
			gridArr[4][5].recShape = player::player.rectangle;

			gridArr[2][2].squareType = box;
			gridArr[1][3].squareType = box;
			gridArr[3][3].squareType = box;

			gridArr[point_1.x][point_1.y].squareType = point;
			gridArr[point_1.x][point_1.y].onPoint = false;
			gridArr[point_2.x][point_2.y].squareType = point;
			gridArr[point_2.x][point_2.y].onPoint = false;
			gridArr[point_3.x][point_3.y].squareType = point;
			gridArr[point_3.x][point_3.y].onPoint = false;


			gridArr[3][2].squareType = notwalkable;
			gridArr[4][2].squareType = notwalkable;
			gridArr[5][2].squareType = notwalkable;

			gridArr[3][4].squareType = notwalkable;
			gridArr[4][4].squareType = notwalkable;
			gridArr[5][4].squareType = notwalkable;
		}
		void initFourth()
		{
			player::player.movimientos = 0;
			exit = false;
			point_1 = { 5,5 };
			point_2 = { 2,3 };
			point_3 = { 1,4 };

			onPointAmount = 0;
			grid.recShape.width = 100;
			grid.recShape.height = 100;
			startPos = { ((GetScreenWidth() * 0.5f) - ((grid.recShape.width * tamX)*0.5f)), ((GetScreenHeight() * 0.5f) - ((grid.recShape.height * tamY)*0.5f)) };

			for (int i = 0; i < tamX; i++)
			{
				for (int j = 0; j < tamY; j++)
				{

					gridArr[i][j].recShape.width = grid.recShape.width;
					gridArr[i][j].recShape.height = grid.recShape.height;

					gridArr[i][j].recShape.x = startPos.x + ((grid.recShape.width)*i);
					gridArr[i][j].recShape.y = startPos.y + ((grid.recShape.height)*j);

					if (i == 0 || i == tamX - 1 || j == 0 || j == tamY - 1)
					{
						gridArr[i][j].squareType = notwalkable;
					}
					else
					{
						gridArr[i][j].squareType = walkable;
					}
					if (i == 1 && j == 5)
					{
						player::player.rectangle = { startPos.x + ((grid.recShape.width)*i) ,startPos.y + ((grid.recShape.height)*j),grid.recShape.width,grid.recShape.height };
					}
				}
			}
			gridArr[1][5].squareType = player;
			gridArr[1][5].recShape = player::player.rectangle;

			gridArr[1][3].squareType = box;
			gridArr[3][3].squareType = box;
			gridArr[4][4].squareType = box;

			gridArr[point_1.x][point_1.y].squareType = point;
			gridArr[point_1.x][point_1.y].onPoint = false;
			gridArr[point_2.x][point_2.y].squareType = point;
			gridArr[point_2.x][point_2.y].onPoint = false;
			gridArr[point_3.x][point_3.y].squareType = point;
			gridArr[point_3.x][point_3.y].onPoint = false;


			gridArr[2][2].squareType = notwalkable;
			gridArr[3][4].squareType = notwalkable;
		}
		void initFifth()
		{
			player::player.movimientos = 0;
			exit = false;
			point_1 = { 3,1 };
			point_2 = { 5,2 };
			point_3 = { 3,4 };

			onPointAmount = 0;
			grid.recShape.width = 100;
			grid.recShape.height = 100;
			startPos = { ((GetScreenWidth() * 0.5f) - ((grid.recShape.width * tamX)*0.5f)), ((GetScreenHeight() * 0.5f) - ((grid.recShape.height * tamY)*0.5f)) };

			for (int i = 0; i < tamX; i++)
			{
				for (int j = 0; j < tamY; j++)
				{

					gridArr[i][j].recShape.width = grid.recShape.width;
					gridArr[i][j].recShape.height = grid.recShape.height;

					gridArr[i][j].recShape.x = startPos.x + ((grid.recShape.width)*i);
					gridArr[i][j].recShape.y = startPos.y + ((grid.recShape.height)*j);

					if (i == 0 || i == tamX - 1 || j == 0 || j == tamY - 1)
					{
						gridArr[i][j].squareType = notwalkable;
					}
					else
					{
						gridArr[i][j].squareType = walkable;
					}
					if (i == 3 && j == 3)
					{
						player::player.rectangle = { startPos.x + ((grid.recShape.width)*i) ,startPos.y + ((grid.recShape.height)*j),grid.recShape.width,grid.recShape.height };
					}
				}
			}
			gridArr[3][3].squareType = player;
			gridArr[3][3].recShape = player::player.rectangle;

			gridArr[2][2].squareType = box;
			gridArr[3][2].squareType = box;
			gridArr[4][3].squareType = box;

			gridArr[point_1.x][point_1.y].squareType = point;
			gridArr[point_1.x][point_1.y].onPoint = false;
			gridArr[point_2.x][point_2.y].squareType = point;
			gridArr[point_2.x][point_2.y].onPoint = false;
			gridArr[point_3.x][point_3.y].squareType = point;
			gridArr[point_3.x][point_3.y].onPoint = false;


			gridArr[4][2].squareType = notwalkable;
			gridArr[2][3].squareType = notwalkable;
			gridArr[4][4].squareType = notwalkable;
		}
	}
}