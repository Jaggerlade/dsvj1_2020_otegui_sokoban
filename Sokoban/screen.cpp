#include "screen.h"
#include "grid.h"
#include "player.h"
#include <iostream>
using namespace std;

namespace Sokoban
{
	namespace screen
	{
		STATE state;
		LEVEL level;
		Texture2D stateArr[15];
		static Rectangle backgroundRec;
		void init()
		{
			state = mainmenu;
			level = level_1;
			loadTexture();
		}
		void update()
		{
			if (state == mainmenu)
			{
				if (IsKeyPressed(KEY_ENTER))
				{
					state = playing;
				}
				else if (IsKeyPressed(KEY_N))
				{
					state = levelSelect;
				}
				else if (IsKeyPressed(KEY_C))
				{
					state = configuration;
				}
				else if (IsKeyPressed(KEY_F))
				{
					state = credits;
				}				
			}
			if (state == credits)
			{
				if (IsKeyPressed(KEY_V))
				{
					state = mainmenu;
				}
			}
			if (state == configuration)
			{
				if (IsKeyPressed(KEY_V))
				{
					state = mainmenu;
				}
			}
			
			if (state == playing)
			{
				if (IsKeyPressed(KEY_P))
				{
					state = pause;
				}
				else if (IsKeyPressed(KEY_R))
				{
					switch (level)
					{
					case Sokoban::screen::level_1:
						grid::initFirst();
						player::initFirst();
						break;
					case Sokoban::screen::level_2:
						grid::initSecond();
						player::initSecond();
						break;
					case Sokoban::screen::level_3:
						grid::initThird();
						player::initThird();
						break;
					case Sokoban::screen::level_4:

						grid::initFourth();
						player::initFourth();
						break;
					case Sokoban::screen::level_5:
						grid::initFifth();
						player::initFifth();
						break;
					default:
						break;
					}
				}
			}
			if (state == levelSelect)
			{
				if (IsKeyPressed(KEY_ONE))
				{
					level = level_1;
					state = playing;
					grid::initFirst();
					player::initFirst();
				}
				else if (IsKeyPressed(KEY_TWO))
				{
					level = level_2;
					state = playing;
					grid::initSecond();
					player::initSecond();
				}
				else if (IsKeyPressed(KEY_THREE))
				{
					level = level_3;
					state = playing;
					grid::initThird();
					player::initThird();
					
				}
				else if (IsKeyPressed(KEY_FOUR))
				{
					state = playing;
					level = level_4;
					grid::initFourth();
					player::initFourth();
					
				}
				else if (IsKeyPressed(KEY_FIVE))
				{
					state = playing;
					level = level_5;
					grid::initFifth();
					player::initFifth();
					
				}
			}
			
			if (state == pause)
			{
				if (IsKeyPressed(KEY_M))
				{
					state = mainmenu;
					switch (level)
					{
					case Sokoban::screen::level_1:
						grid::initFirst();
						player::initFirst();
						break;
					case Sokoban::screen::level_2:
						grid::initSecond();
						player::initSecond();
						break;
					case Sokoban::screen::level_3:
						grid::initThird();
						player::initThird();
						break;
					case Sokoban::screen::level_4:
						
						grid::initFourth();
						player::initFourth();
						break;
					case Sokoban::screen::level_5:
						grid::initFifth();
						player::initFifth();
						break;
					default:
						break;
					}
					
				}
				else if (IsKeyPressed(KEY_Z))
				{
					state = playing;
				}
			}
			if (state == victory)
			{
				switch (level)
				{
				case Sokoban::screen::level_1:
					if (IsKeyPressed(KEY_N))
					{
						state = playing;
						level = level_2;
						grid::initSecond();
						player::initSecond();						
					}
					break;
				case Sokoban::screen::level_2:
					if (IsKeyPressed(KEY_N))
					{
						level = level_3;
						grid::initThird();
						player::initThird();
						state = playing;
					}
					break;
				case Sokoban::screen::level_3:
					if (IsKeyPressed(KEY_N))
					{
						level = level_4;
						grid::initFourth();
						player::initFourth();
						state = playing;
					}
					break;
				case Sokoban::screen::level_4:
					if (IsKeyPressed(KEY_N))
					{
						level = level_5;
						grid::initFifth();
						player::initFifth();
						state = playing;
					}
					break;
				case Sokoban::screen::level_5:
					if (IsKeyPressed(KEY_M))
					{
						state = mainmenu;
						level = level_1;
						grid::initFirst();
						player::initFirst();
					}
					
					break;
				default:
					break;
				}
			}
		}
		void draw()
		{
			if (state == mainmenu)
			{
				DrawTexture(stateArr[playing],(GetScreenWidth()/2)-100,GetScreenHeight()/2-200,WHITE);
				DrawTexture(stateArr[levelSelect], (GetScreenWidth() / 2)-100, GetScreenHeight()/2-100, WHITE);
				DrawTexture(stateArr[configuration], (GetScreenWidth() / 2)-100, GetScreenHeight()/2, WHITE);
				DrawTexture(stateArr[credits], (GetScreenWidth() / 2)-100, GetScreenHeight()/2 + 100, WHITE);
				DrawTexture(stateArr[exit], (GetScreenWidth() / 2) - 100, GetScreenHeight() / 2 + 200, WHITE);
			}
			
			else if (state == pause)
			{
				DrawTexture(stateArr[5], (GetScreenWidth() / 2) - 100, GetScreenHeight() / 2 - 100, WHITE);
				DrawTexture(stateArr[mainmenu], (GetScreenWidth() / 2) - 100, GetScreenHeight() / 2, WHITE);
			}

			else if (state == configuration)
			{
				DrawTexture(stateArr[6], (GetScreenWidth() * 6 / 8), GetScreenHeight() *6/8, WHITE);
			}
			else if (state == credits)
			{
				DrawTexture(stateArr[6], (GetScreenWidth() * 6 / 8) , GetScreenHeight() *6/8, WHITE);
			}
			else if (state == victory)
			{
				if (level != level_5)
				{
					DrawTexture(stateArr[7], (GetScreenWidth() / 2) - 100, GetScreenHeight() / 2 - 100, WHITE);
				}
				else
				{
					DrawTexture(stateArr[mainmenu], (GetScreenWidth() / 2) - 100, GetScreenHeight() / 2 - 100, WHITE);
					DrawTexture(stateArr[exit], (GetScreenWidth() / 2) - 100, GetScreenHeight() / 2, WHITE);
				}
			}
			else if (state == playing)
			{
				DrawTexture(stateArr[9], ((GetScreenWidth() * 6 / 8) + 50), GetScreenHeight() * 1 / 8, WHITE);
				DrawText(FormatText("Movimientos: %i", player::player.movimientos), GetScreenWidth()/20, GetScreenHeight() * 1 / 8, 30, BLACK);
			}
			else if (state == levelSelect)
			{
				DrawTexture(stateArr[10], ((GetScreenWidth() *1/6)-50), GetScreenHeight() / 2, WHITE);
				DrawTexture(stateArr[11], ((GetScreenWidth() * 2 / 6) - 50), GetScreenHeight() / 2, WHITE);
				DrawTexture(stateArr[12], ((GetScreenWidth() * 3 / 6) - 50) , GetScreenHeight() / 2, WHITE);
				DrawTexture(stateArr[13], ((GetScreenWidth() * 4 / 6) - 50), GetScreenHeight() / 2, WHITE);
				DrawTexture(stateArr[14], ((GetScreenWidth() * 5 / 6) - 50), GetScreenHeight() / 2, WHITE);
			}
		}
		void deInit()
		{
			unloadTexture();
		}
		void loadTexture()
		{
			Image reSize;

			reSize = LoadImage("res/Assets/Buttons/Play.png");
			ImageResize(&reSize, 300, 100);
			stateArr[playing] = LoadTextureFromImage(reSize);
			UnloadImage(reSize);

			reSize = LoadImage("res/Assets/Buttons/Config.png");
			ImageResize(&reSize, 300, 100);
			stateArr[configuration] = LoadTextureFromImage(reSize);
			UnloadImage(reSize);

			reSize = LoadImage("res/Assets/Buttons/Credits.png");
			ImageResize(&reSize, 300, 100);
			stateArr[credits] = LoadTextureFromImage(reSize);
			UnloadImage(reSize);

			reSize = LoadImage("res/Assets/Buttons/Exit.png");
			ImageResize(&reSize, 300, 100);
			stateArr[exit] = LoadTextureFromImage(reSize);
			UnloadImage(reSize);
			
			reSize = LoadImage("res/Assets/Buttons/Menu.png");
			ImageResize(&reSize, 300, 100);
			stateArr[mainmenu] = LoadTextureFromImage(reSize);
			UnloadImage(reSize);

			reSize = LoadImage("res/Assets/Buttons/Continue.png");
			ImageResize(&reSize, 300, 100);
			stateArr[5] = LoadTextureFromImage(reSize);
			UnloadImage(reSize);

			reSize = LoadImage("res/Assets/Buttons/Return.png");
			ImageResize(&reSize, 250, 100);
			stateArr[6] = LoadTextureFromImage(reSize);
			UnloadImage(reSize);

			reSize = LoadImage("res/Assets/Buttons/NextLevel.png");
			ImageResize(&reSize, 250, 100);
			stateArr[victory] = LoadTextureFromImage(reSize);
			UnloadImage(reSize);

			reSize = LoadImage("res/Assets/Buttons/LevelSelection.png");
			ImageResize(&reSize, 300, 100);
			stateArr[levelSelect] = LoadTextureFromImage(reSize);
			UnloadImage(reSize);

			reSize = LoadImage("res/Assets/Buttons/Reset.png");
			ImageResize(&reSize, 250, 100);
			stateArr[9] = LoadTextureFromImage(reSize);
			UnloadImage(reSize);

			reSize = LoadImage("res/Assets/Buttons/Level_1.png");
			ImageResize(&reSize, 150, 100);
			stateArr[10] = LoadTextureFromImage(reSize);
			UnloadImage(reSize);

			reSize = LoadImage("res/Assets/Buttons/Level_2.png");
			ImageResize(&reSize, 150, 100);
			stateArr[11] = LoadTextureFromImage(reSize);
			UnloadImage(reSize);

			reSize = LoadImage("res/Assets/Buttons/Level_3.png");
			ImageResize(&reSize, 150, 100);
			stateArr[12] = LoadTextureFromImage(reSize);
			UnloadImage(reSize);

			reSize = LoadImage("res/Assets/Buttons/Level_4.png");
			ImageResize(&reSize, 150, 100);
			stateArr[13] = LoadTextureFromImage(reSize);
			UnloadImage(reSize);

			reSize = LoadImage("res/Assets/Buttons/Level_5.png");
			ImageResize(&reSize, 150, 100);
			stateArr[14] = LoadTextureFromImage(reSize);
			UnloadImage(reSize);
		}
		void unloadTexture()
		{
			for (int i = 0; i < 15; i++)
			{
				UnloadTexture(stateArr[i]);
			}
		}
	}
}