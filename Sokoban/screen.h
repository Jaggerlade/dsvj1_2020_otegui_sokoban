#ifndef SCREEN_H
#define SCREEN_H
#include "raylib.h"
namespace Sokoban
{
	namespace screen
	{
		enum LEVEL
		{
			level_1,
			level_2,
			level_3,
			level_4,
			level_5
		};
		enum STATE
		{
			playing,
			configuration,
			credits,
			exit,
			mainmenu,
			pause,
			back,
			victory,
			levelSelect
		};
		extern STATE state;
		extern LEVEL level;
		extern Texture2D stateArr[15];
		void init();
		void update();
		void draw();
		void deInit();
		void loadTexture();
		void unloadTexture();
	}
}
#endif // !GRID_H#pragma once
