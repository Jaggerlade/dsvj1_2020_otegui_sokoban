#ifndef GAME_LOOP
#define GAME_LOOP
#include "player.h"
namespace Sokoban
{
	namespace game_loop 
	{
		void init();
		void update();
		void draw();
		void deInit();
		void game();
		void reInit();
	}
}
#endif	//!GAME_LOOP