#include "game_loop.h"
#include "grid.h"
#include "screen.h"
#include <iostream>
using namespace std;
namespace Sokoban
{
	namespace game_loop
	{
		void init()
		{
			SetTargetFPS(60);
			screen::init();
			grid::init();
			player::init();
			
		}
		void update()
		{
			screen::update();
			player::update();
			grid::update();
			//cout << "estado: " << screen::state << endl;
			//cout << "nivel: " << screen::level << endl;
		}
		void draw()
		{
			ClearBackground(BACKGROUND);
			BeginDrawing();
			screen::draw();
			grid::draw();
			player::draw();

			EndDrawing();
		}
		void deInit()
		{
			screen::deInit();
			grid::deInit();
			player::deInit();
		}
		void game()
		{
			update();
			draw();
		}
		void reInit()
		{

		}
	}
}

