#include "raylib.h"
#include "game_loop.h"
using namespace Sokoban;
int main(void)
{
	int screenWidth = 1280;
	int screenHeight = 720;
	InitWindow(screenWidth, screenHeight, "Consola");
	game_loop::init();
	while (!WindowShouldClose())
	{
		game_loop::game();
	}
	game_loop::deInit();
	CloseWindow();
	return 0;
}